# Fuzz'em all library

## Overview

A Maven library to help you test your methods for bogus input. It is easily applied to your project, and can go through Spring injections aswell (for Java8 only atm).

As opposed to other Java fuzzing libraries requiring you to write a unit test per method you want to check, this library allows you to test entire packages, and dynamically generates unit tests matching your classes / methods.

The primary goal of this library is to test all your methods with limit-checking cases for all parameters.

As of 1.0, the following parameter types are taken into account :
- boolean
- byte
- char
- double
- int
- float
- long
- short
- java.lang.Byte
- java.lang.Character
- java.lang.Double
- java.lang.Float
- java.lang.Integer
- java.lang.Long
- java.lang.Short
- java.lang.String

All other object types will only be tested for null.

## Add the library to your project

### Gradle :

    implementation 'io.gitlab.secudev:fuzz-em-all:1.0'

### Maven :

    <dependency>
        <groupId>io.gitlab.secudev</groupId>
        <artifactId>fuzz-em-all</artifactId>
        <version>1.0</version>
    </dependency>
   
## Run tests on your methods

### Without injections framework :

    @RunWith(FuzzerRunner.class)
    @FuzzingPackageCheck(packageName = "io.gitlab.secudev.fuzzer.data")
    public class TestFuzzerRunner {

    }

### With Spring :

The trick is using Spring JUnit's runner to activate the injection framework for each of the classes we'll test. Problem is, it's designed to activate only on the unit test class. In the prerequisites for this init, the unit test class must have at least one method with the @Test annotation. We'll have to create a dummy one.
We also need a ContextConfiguration to load the Spring context. This can be supplied with the @ContextConfiguration annotation on the test class, along with the @ComponentScan annotation.
Both ContextConfiguration & ComponentScan annotations present on the test class will be dynamically added to any class we'll try to fuzz.
Here's the basic class we'd need to add to test a group of Spring framework-based classes, including injections on some of the variables :

    @RunWith(FuzzerSpringRunner.class)
    @ComponentScan("io.gitlab.secudev.fuzzer.spring")
    @FuzzingPackageCheck(packageName = "io.gitlab.secudev.fuzzer.spring.test")
    @ContextConfiguration(classes = TestSpringConfig.class)
    public class TestSpringFuzzerRunner {
    
        @Test
        public void doNothing() {
            System.out.println("");
        }
    
    }

Context configuration class example :

    @Configuration
    @ComponentScan("io.gitlab.secudev.fuzzer")
    public class TestSpringConfig {
    }

## Security status

This library is tested for known vulnerabilities with the OWASP Dependency Check tool. If you find any vulnerability, I'll try to fix them asap if possible.

## License

This library is provided under the [MIT license](http://www.opensource.org/licenses/mit-license.php).

## Contribute

If you have any bug, suggestion, comment, feel free to contribute, do a fork, merge requests up to your liking, I'll review them whenever possible !