package io.gitlab.secudev.fuzzer;

import org.junit.runner.Description;
import org.junit.runner.Runner;
import org.junit.runner.notification.RunNotifier;

public class FuzzerRunner extends Runner {

    private Class testClass;

    public FuzzerRunner(final Class testClass) {
        super();
        this.testClass = testClass;
    }

    @Override
    public Description getDescription() {
        return Description
                .createTestDescription(testClass, "Testing fuzzing behaviour on all methods of the class");
    }

    @Override
    public void run(final RunNotifier notifier) {
        FuzzerRunnerHelper.run(notifier, testClass, null, null);
    }
}
