package io.gitlab.secudev.fuzzer;

import io.gitlab.secudev.fuzzer.generator.ByteGenerator;
import io.gitlab.secudev.fuzzer.generator.CharGenerator;
import io.gitlab.secudev.fuzzer.generator.DoubleGenerator;
import io.gitlab.secudev.fuzzer.generator.FloatGenerator;
import io.gitlab.secudev.fuzzer.generator.ShortGenerator;
import io.gitlab.secudev.fuzzer.generator.StringGenerator;
import io.gitlab.secudev.fuzzer.generator.IntGenerator;
import io.gitlab.secudev.fuzzer.generator.LongGenerator;
import org.junit.runner.Description;
import org.junit.runner.notification.Failure;
import org.junit.runner.notification.RunNotifier;

import java.io.File;
import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.List;
import java.util.function.Consumer;

public class FuzzerRunnerHelper {

    private FuzzerRunnerHelper() {
        // Nothing to do here
    }

    protected static Class[] getClasses(final String packageName) throws ClassNotFoundException, IOException {
        final ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
        assert classLoader != null;
        final String path = packageName.replace('.', '/');
        final Enumeration<URL> resources = classLoader.getResources(path);
        final List<File> dirs = new ArrayList<>();
        while (resources.hasMoreElements()) {
            final URL resource = resources.nextElement();
            dirs.add(new File(resource.getFile()));
        }
        final ArrayList<Class> classes = new ArrayList<>();
        for (final File directory : dirs) {
            classes.addAll(findClasses(directory, packageName));
        }
        return classes.toArray(new Class[classes.size()]);
    }

    /**
     * Recursive method used to find all classes in a given directory and subdirs.
     *
     * @param directory   The base directory
     * @param packageName The package name for classes found inside the base directory
     * @return The classes
     * @throws ClassNotFoundException
     */
    private static List<Class> findClasses(final File directory, final String packageName)
            throws ClassNotFoundException {
        final List<Class> classes = new ArrayList<>();
        if (!directory.exists()) {
            return classes;
        }
        final File[] files = directory.listFiles();
        for (final File file : files) {
            if (file.isDirectory()) {
                assert !file.getName().contains(".");
                classes.addAll(findClasses(file, packageName + "." + file.getName()));
            } else if (file.getName().endsWith(".class")) {
                classes.add(Class
                        .forName(packageName + '.' + file.getName().substring(0, file.getName().length() - 6)));
            }
        }
        return classes;
    }


    private static List<Exception> invoke(final Method method, final Object instance, Object[][] parameters) {
        final List<Exception> result = new ArrayList<>();
        Object[] localRun = new Object[parameters.length];
        int[] sizes = new int[parameters.length];
        int[] indexes = new int[parameters.length];
        for (int i = 0; i < indexes.length; i++) {
            indexes[i] = 0;
            localRun[i] = parameters[i][0];
            sizes[i] = parameters[i].length;
        }
        int limit = parameters[0].length;
        while (indexes[0] < limit) {

            try {
                method.invoke(instance, localRun);
            } catch (final Exception e) {
                final Exception added = new Exception("At fault: " + Arrays.toString(localRun), e.getCause());
                result.add(added);
                System.out.println("At fault: " + Arrays.toString(localRun));
                e.getCause().printStackTrace();
            }
            increase(indexes, sizes);
            if (indexes[0] < limit) {
                for (int i = 0; i < indexes.length; i++) {
                    localRun[i] = parameters[i][indexes[i]];
                }
            }
        }
        return result;
    }

    private static void increase(final int[] index, final int[] sizes) {
        index[index.length - 1]++;
        for (int i = index.length - 1; i > 0; i--) {
            if (index[i] == sizes[i]) {
                index[i] = 0;
                index[i - 1]++;
            }
        }
    }

    protected static List<Class> getTestClasses(final Class testClass) {
        final List<Class> result = new ArrayList<>();
        try {
            for (final Annotation annotatedType : testClass.getAnnotations()) {
                if (annotatedType instanceof FuzzingPackageCheck) {
                    System.out
                            .println("Should check package " + ((FuzzingPackageCheck) annotatedType).packageName());
                    final Class[] classes = FuzzerRunnerHelper.getClasses(((FuzzingPackageCheck) annotatedType).packageName());
                    for (final Class clazz : classes) {
                        if (!clazz.getSimpleName().startsWith("Test")
                                && !clazz.getSimpleName().endsWith("Test")
                                && !((FuzzingPackageCheck) annotatedType).excludeClasses().contains(clazz.getName())) {
                            if (clazz.isAnnotation() || clazz.isEnum() || clazz.isInterface() ||
                                    Modifier.isPrivate(clazz.getModifiers())) {
                                continue;
                            }

                            boolean addClass = false;
                            for (final Method method : clazz.getDeclaredMethods()) {
                                if (method.getParameterCount() > 0) {
                                    addClass = true;
                                    break;
                                }
                            }
                            if (addClass) {
                                result.add(clazz);
                            }
                        }
                    }
                }
            }
        } catch (final Exception e) {
            throw new RuntimeException(e);
        }

        return result;
    }

    protected static void testMethod(final Object instance, final RunNotifier notifier, final Method method) {
        if (method.getParameterCount() > 0) {
            boolean privateMethod = false;

            if (method.isAccessible()) {
                privateMethod = true;
                method.setAccessible(true);
            }

            final String nameTest =
                    "testNPE_" + instance.getClass().getSimpleName() + "_" + method.getName() +
                            (privateMethod ? "_private" : "");

            notifier.fireTestStarted(Description.createTestDescription(instance.getClass(),
                    nameTest));
            final Object[][] randomParameters = new Object[method.getParameterCount()][];
            for (int i = 0; i < randomParameters.length; i++) {
                Class<?> localClazz = method.getParameterTypes()[i];
                randomParameters[i] = new Object[]{null};
                switch (localClazz.getName()) {
                    case "boolean":
                        randomParameters[i] = new Boolean[]{Boolean.TRUE, Boolean.FALSE};
                        break;
                    case "byte":
                        randomParameters[i] = new ByteGenerator().generateDataset(false);
                        break;
                    case "char":
                        randomParameters[i] = new CharGenerator().generateDataset(false);
                        break;
                    case "double":
                        randomParameters[i] = new DoubleGenerator().generateDataset(false);
                        break;
                    case "int":
                        randomParameters[i] = new IntGenerator().generateDataset(false);
                        break;
                    case "float":
                        randomParameters[i] = new FloatGenerator().generateDataset(false);
                        break;
                    case "long":
                        randomParameters[i] = new LongGenerator().generateDataset(false);
                        break;
                    case "short":
                        randomParameters[i] = new ShortGenerator().generateDataset(false);
                        break;
                    case "java.lang.Byte":
                        randomParameters[i] = new ByteGenerator().generateDataset(true);
                        break;
                    case "java.lang.Character":
                        randomParameters[i] = new CharGenerator().generateDataset(true);
                        break;
                    case "java.lang.Double":
                        randomParameters[i] = new DoubleGenerator().generateDataset(true);
                        break;
                    case "java.lang.Float":
                        randomParameters[i] = new FloatGenerator().generateDataset(true);
                        break;
                    case "java.lang.Integer":
                        randomParameters[i] = new IntGenerator().generateDataset(true);
                        break;
                    case "java.lang.Long":
                        randomParameters[i] = new LongGenerator().generateDataset(true);
                        break;
                    case "java.lang.Short":
                        randomParameters[i] = new ShortGenerator().generateDataset(true);
                        break;
                    case "java.lang.String":
                        randomParameters[i] = new StringGenerator().generateDataset(true);
                        break;
                    default:
                        randomParameters[i] = new Object[] { null };
                }
            }

            List<Exception> resultExceptions = invoke(method, instance, randomParameters);

            if (privateMethod) {
                method.setAccessible(false);
            }

            if (!resultExceptions.isEmpty()) {
                final Failure failure =
                        new Failure(Description.createTestDescription(instance.getClass(), nameTest),
                                resultExceptions.get(0));
                notifier.fireTestFailure(failure);
            } else {
                notifier.fireTestFinished(Description.createTestDescription(instance.getClass(),
                        nameTest));
            }
        }

    }

    protected static void run(final RunNotifier notifier,
                              final Class testClass,
                              final Consumer preInstanciation,
                              final Consumer postInstanciation) {
        System.out.println("Running fuzzing checks defined by class " + testClass);
        final List<Class> classes = getTestClasses(testClass);
        for (final Class clazz : classes) {
            if (preInstanciation != null) {
                preInstanciation.accept(clazz);
            }
            Object instance = null;
            try {
                instance = clazz.newInstance();
            } catch (final InstantiationException | IllegalAccessException e) {
                // Unable to create class to test
                notifier
                        .fireTestIgnored(Description.createTestDescription(testClass, "testNPE_" + clazz.getSimpleName() + "_unableToTest"));
                continue;
            }

            if (postInstanciation != null) {
                postInstanciation.accept(instance);
            }

            System.out
                    .println("Testing " + clazz.getCanonicalName());

            for (final Method method : clazz.getDeclaredMethods()) {
                testMethod(instance, notifier, method);
            }
        }
    }
}
