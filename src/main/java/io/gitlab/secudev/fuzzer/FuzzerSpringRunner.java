package io.gitlab.secudev.fuzzer;

import org.junit.runner.Description;
import org.junit.runner.notification.RunNotifier;
import org.junit.runners.model.InitializationError;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestContextManager;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.lang.annotation.Annotation;

public class FuzzerSpringRunner extends SpringJUnit4ClassRunner {

    private final Class testClass;

    public FuzzerSpringRunner(final Class testClass) throws InitializationError {
        super(testClass);
        this.testClass = testClass;
    }

    @Override
    public Description getDescription() {
        return Description
                .createTestDescription(testClass, "Testing fuzzing behaviour on all methods of the class");
    }

    @Override
    public void run(final RunNotifier notifier) {
        final Annotation[] mainAnnotations = testClass.getAnnotations();

        FuzzerRunnerHelper.run(notifier,
                testClass,
                null,
                instance -> {
                    // Adding ContextConfiguration & ComponentScan annotations from the main test class back to the tested class
                    // to correctly initiate the Spring components injection.
                    // Warning: RuntimeAnnotations class might only work in Java8
                    for (final Annotation a : mainAnnotations) {
                        if (a instanceof ContextConfiguration && instance.getClass().getAnnotation(ContextConfiguration.class) == null) {
                            RuntimeAnnotations.putAnnotation(instance.getClass(), ContextConfiguration.class, (ContextConfiguration) a);
                        } else if (a instanceof ComponentScan && instance.getClass().getAnnotation(ComponentScan.class) == null) {
                            RuntimeAnnotations.putAnnotation(instance.getClass(), ComponentScan.class, (ComponentScan) a);
                        }
                    }

                    final TestContextManager contextManager = new TestContextManager(instance.getClass());
                    try {
                        contextManager.prepareTestInstance(instance);
                    } catch (final Exception e) {
                        throw new RuntimeException(e);
                    }
                });
    }
}
