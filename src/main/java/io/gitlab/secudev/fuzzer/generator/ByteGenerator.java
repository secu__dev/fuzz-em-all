package io.gitlab.secudev.fuzzer.generator;

import java.util.ArrayList;
import java.util.List;

public class ByteGenerator implements Generator<Byte> {

    @Override
    public Byte[] generateDataset(boolean nullable) {
        final List<Byte> result = new ArrayList<>();

        if (nullable) {
            result.add(null);
        }
        for (byte b = Byte.MIN_VALUE ; b < Byte.MAX_VALUE ; b++) {
            result.add(b);
        }
        result.add(Byte.MAX_VALUE);

        final Byte[] r = new Byte[result.size()];
        return result.toArray(r);
    }
}
