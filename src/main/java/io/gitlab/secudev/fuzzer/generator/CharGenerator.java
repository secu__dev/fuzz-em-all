package io.gitlab.secudev.fuzzer.generator;

import java.util.ArrayList;
import java.util.List;

public class CharGenerator implements Generator<Character> {

    @Override
    public Character[] generateDataset(boolean nullable) {
        final List<Character> result = new ArrayList<>();

        if (nullable) {
            result.add(null);
        }
        for (char b = Character.MIN_VALUE ; b < Character.MAX_VALUE ; b++) {
            result.add(b);
        }
        result.add(Character.MAX_VALUE);

        final Character[] r = new Character[result.size()];
        return result.toArray(r);
    }
}
