package io.gitlab.secudev.fuzzer.generator;

import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.List;

public class DoubleGenerator implements Generator<Double> {

    @Override
    public Double[] generateDataset(boolean nullable) {
        final List<Double> result = new ArrayList<>();

        if (nullable) {
            result.add(null);
        }

        result.add(0.0);
        result.add(-1.0);
        result.add(1.0);
        result.add(Double.NaN);
        result.add(Double.POSITIVE_INFINITY);
        result.add(Double.NEGATIVE_INFINITY);

        result.add(Double.MIN_VALUE);
        result.add(-Double.MIN_VALUE);
        result.add(Double.MAX_VALUE);
        result.add(-Double.MAX_VALUE);

        final SecureRandom random = new SecureRandom();
        for (int i = 0 ; i < DATASET_RANDOM_SIZE ; i++) {
            result.add(random.nextDouble());
        }

        final Double[] r = new Double[result.size()];
        return result.toArray(r);
    }
}
