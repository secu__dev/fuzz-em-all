package io.gitlab.secudev.fuzzer.generator;

import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.List;

public class FloatGenerator implements Generator<Float> {

    @Override
    public Float[] generateDataset(boolean nullable) {
        final List<Float> result = new ArrayList<>();

        if (nullable) {
            result.add(null);
        }

        result.add(0.0f);
        result.add(-1.0f);
        result.add(1.0f);
        result.add(Float.NaN);
        result.add(Float.POSITIVE_INFINITY);
        result.add(Float.NEGATIVE_INFINITY);

        result.add(Float.MIN_VALUE);
        result.add(-Float.MIN_VALUE);
        result.add(Float.MAX_VALUE);
        result.add(-Float.MAX_VALUE);

        final SecureRandom random = new SecureRandom();
        for (int i = 0 ; i < DATASET_RANDOM_SIZE ; i++) {
            result.add(random.nextFloat());
        }

        final Float[] r = new Float[result.size()];
        return result.toArray(r);
    }
}
