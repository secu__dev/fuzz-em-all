package io.gitlab.secudev.fuzzer.generator;

public interface Generator<T> {

    int DATASET_RANDOM_SIZE = 50;

    T[] generateDataset(boolean nullable);
}
