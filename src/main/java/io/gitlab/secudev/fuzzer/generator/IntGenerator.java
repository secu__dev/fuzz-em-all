package io.gitlab.secudev.fuzzer.generator;

import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.List;

public class IntGenerator implements Generator<Integer> {

    @Override
    public Integer[] generateDataset(boolean nullable) {
        final List<Integer> result = new ArrayList<>();

        if (nullable) {
            result.add(null);
        }
        result.add(Integer.MAX_VALUE);
        result.add(Integer.MIN_VALUE);
        result.add(0);
        result.add(1);
        result.add(-1);

        final SecureRandom random = new SecureRandom();
        for (int i = 0; i < DATASET_RANDOM_SIZE; i++) {
            result.add(random.nextInt());
        }
        final Integer[] r = new Integer[result.size()];
        return result.toArray(r);
    }
}
