package io.gitlab.secudev.fuzzer.generator;

import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.List;

public class LongGenerator implements Generator<Long> {

    @Override
    public Long[] generateDataset(boolean nullable) {
        final List<Long> result = new ArrayList<>();

        if (nullable) {
            result.add(null);
        }
        result.add(Long.MAX_VALUE);
        result.add(Long.MIN_VALUE);
        result.add(0L);
        result.add(1L);
        result.add(-1L);

        final SecureRandom random = new SecureRandom();
        for (int i = 0; i < DATASET_RANDOM_SIZE; i++) {
            result.add(random.nextLong());
        }
        final Long[] r = new Long[result.size()];
        return result.toArray(r);
    }
}
