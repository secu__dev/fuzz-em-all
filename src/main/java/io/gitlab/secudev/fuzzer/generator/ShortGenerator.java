package io.gitlab.secudev.fuzzer.generator;

import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.List;

public class ShortGenerator implements Generator<Short> {

    @Override
    public Short[] generateDataset(boolean nullable) {
        final List<Short> result = new ArrayList<>();

        if (nullable) {
            result.add(null);
        }

        result.add((short) 0);
        result.add((short) -1);
        result.add((short) 1.0);

        result.add(Short.MIN_VALUE);
        result.add(Short.MAX_VALUE);

        final SecureRandom random = new SecureRandom();
        for (int i = 0; i < DATASET_RANDOM_SIZE; i++) {
            result.add((short) (random.nextInt(Short.MAX_VALUE - Short.MIN_VALUE) + Short.MIN_VALUE));
        }

        final Short[] r = new Short[result.size()];
        return result.toArray(r);

    }
}
