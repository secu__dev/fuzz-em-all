package io.gitlab.secudev.fuzzer.generator;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

public class StringGenerator implements Generator<String> {

    private List<String> loadFile(final String resourceFilename) throws IOException {
        final List<String> result = new ArrayList<>();

        final InputStream resourceStream = getClass().getClassLoader().getResourceAsStream(resourceFilename);
        if (resourceStream == null) {
            return result;
        }
        final BufferedReader is = new BufferedReader(new InputStreamReader(resourceStream, StandardCharsets.UTF_8));
        String line;
        while ((line = is.readLine()) != null) {
            if (!line.startsWith("#")) {
                result.add(line);
            }
        }

        return result;
    }

    @Override
    public String[] generateDataset(boolean nullable) {
        final List<String> result = new ArrayList<>();

        if (nullable) {
            result.add(null);
        }

        result.add("");
        result.add("#");
        try {
            result.addAll(loadFile("fuzzing/all-attacks-unix.txt"));
            result.addAll(loadFile("fuzzing/all-attacks-win.txt"));
            result.addAll(loadFile("fuzzing/all-attacks-xplatform.txt"));
            result.addAll(loadFile("fuzzing/big-list-of-naughty-strings.txt"));
        } catch (final IOException e) {
            // Unable to load resource files
            e.printStackTrace();
        }

        final String[] r = new String[result.size()];
        return result.toArray(r);
    }
}
