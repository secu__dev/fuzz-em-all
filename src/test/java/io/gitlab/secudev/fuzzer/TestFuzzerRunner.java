package io.gitlab.secudev.fuzzer;

import org.junit.runner.RunWith;

@RunWith(FuzzerRunner.class)
@FuzzingPackageCheck(packageName = "io.gitlab.secudev.fuzzer.data")
public class TestFuzzerRunner {

}
