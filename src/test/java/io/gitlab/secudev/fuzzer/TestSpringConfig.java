package io.gitlab.secudev.fuzzer;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("io.gitlab.secudev.fuzzer")
public class TestSpringConfig {
}
