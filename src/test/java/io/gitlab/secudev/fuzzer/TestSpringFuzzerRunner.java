package io.gitlab.secudev.fuzzer;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.ContextConfiguration;


@RunWith(FuzzerSpringRunner.class)
@ComponentScan("io.gitlab.secudev.fuzzer.spring")
@FuzzingPackageCheck(packageName = "io.gitlab.secudev.fuzzer.spring.test")
@ContextConfiguration(classes = TestSpringConfig.class)
public class TestSpringFuzzerRunner {

    @Test
    public void doNothing() {
        System.out.println("");
    }

}
