package io.gitlab.secudev.fuzzer.data;

public class NpeThrowing {

  public int compute(final Integer a, final Integer b) {
    if (a == null || b == null) {
      return 0;
    }
    //System.out.println(a.intValue() + b.intValue());
    return a.intValue() + b.intValue();
  }

  public int protectedCompute(final Integer a, final Integer b) {
    return (a == null ? 0 : a.intValue()) + (b == null ? 0 : b.intValue());
  }

  public int weakCompute(final Integer a) {
    return a.intValue();
  }

  public void uselessTest(final boolean b) {
    return;
  }

  public void testBooleanInt(final boolean b, final int a, final Integer abc) {
    return;
  }

  public void testRandom(final String a, final Long b, final short c) {
    return;
  }
}
