package io.gitlab.secudev.fuzzer.spring.test;

import io.gitlab.secudev.fuzzer.TestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UseService {

    @Autowired
    TestService service;

    public boolean test(final String input) {
        return service.isValid(input);
    }
}
